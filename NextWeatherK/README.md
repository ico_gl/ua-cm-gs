# IPMA API client demo - https://api.ipma.pt/
IPMA is the public institute responsible for weather forecasting in Portugal.

## Endpoints

Getting the next 5-day forecast for a city with id #1131200:
https://api.ipma.pt/open-data/forecast/meteorology/cities/daily/1131200.json

List of Portuguese cities with Ids:
https://api.ipma.pt/open-data/distrits-islands.json

List of weather conditions descriptive labels:
https://api.ipma.pt/open-data/weather-type-classe.json


## Notes
* implemented in Kotlin
* uses Retrofit + Moshi (object-JSON converter)
* uses coroutines to call remote (HTTP) endpoints
