package pt.ua.weatherk.data

data class City (
    val local: String,
    val globalIdLocal: Int,
    val latitude: Double,
    val longitude: Double
    //val forecasts: List<Weather>
)