package pt.ua.weatherk.data

data class TypesOfWeatherCollection (val owner:String,
                                     val data: List<WeatherType>)