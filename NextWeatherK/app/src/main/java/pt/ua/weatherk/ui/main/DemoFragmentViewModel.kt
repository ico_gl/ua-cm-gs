package pt.ua.weatherk.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import pt.ua.weatherk.data.CitiesCollectionRoot
import pt.ua.weatherk.data.ForecastsForLocationGroup
import pt.ua.weatherk.network.IpmaApi


class DemoFragmentViewModel : ViewModel() {

    // The internal MutableLiveData String that stores the most recent response
    private val _response = MutableLiveData<String>()

    // The external immutable LiveData for the response String
    val response: LiveData<String>
        get() = _response

    init {
        // updateCitiesList()
    }

     fun updateForecastForCity() {
        val PORTO_ID = 1131200
        viewModelScope.launch {
            try {
                val forecastResult: ForecastsForLocationGroup = IpmaApi.retrofitService.getWeatherForCity( PORTO_ID )
                _response.value = "Success: got forecast fro next ${forecastResult.data.size} days. E.g.: Tomorrow " +
                        "${forecastResult.data.get(1)}"
            } catch (e: Exception) {
                _response.value = "Failure: ${e.message}"
            }
        }
    }

    fun updateCitiesList() {
        viewModelScope.launch {
            try {
                val listResult: CitiesCollectionRoot = IpmaApi.retrofitService.getListOfCities()
                _response.value = "Success: found ${listResult.data.size} cities. E.g.: ${listResult.data.get(17) } } "
            } catch (e: Exception) {
                _response.value = "Failure: ${e.message}"
            }
        }
    }

    fun updateWeatherConditionsLabels() {
        viewModelScope.launch {
            try {
                val listOfLabels = IpmaApi.retrofitService.getWeatherTypes()
                _response.value = "Success: found ${listOfLabels.data.size} labels. E.g.: ${listOfLabels.data.get(2) } } "
            } catch (e: Exception) {
                _response.value = "Failure: ${e.message}"
            }
        }
    }

    /**
     *  just for reference; an alternative implementation without coroutines
         private fun updateCitiesListCallback() {
        IpmaApi.retrofitService.getListOfCities().enqueue(
            object: Callback<CitiesCollectionRoot> {
                override fun onResponse(
                    call: Call<CitiesCollectionRoot>,
                    response: Response<CitiesCollectionRoot>
                ) {
                    Log.i("IPMA", "Got response for cities")
                    _response.value = "Sucess: " + response.body()
                }

                override fun onFailure(call: Call<CitiesCollectionRoot>, t: Throwable) {
                    Log.i("IPMA", "Got failure for cities")
                    _response.value = "Failure: " + t.message
                }
            })

        Log.i("IPMA", "\tCall enqueued...")
    }
*/

}