/*
 * Copyright 2019, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package pt.ua.weatherk.network

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import pt.ua.weatherk.data.CitiesCollectionRoot
import pt.ua.weatherk.data.ForecastsForLocationGroup
import pt.ua.weatherk.data.TypesOfWeatherCollection
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

private const val BASE_URL = "https://api.ipma.pt/"

private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()


private val retrofit = Retrofit.Builder()
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .baseUrl(BASE_URL)
    .build()

interface MappedIpmaApiService {

    @GET("open-data/distrits-islands.json")
    //fun getListOfCities(): Call<CitiesCollectionRoot>
    suspend fun getListOfCities(): CitiesCollectionRoot

    @GET("open-data/forecast/meteorology/cities/daily/{localId}.json")
    //fun getWeatherParent(@Path("localId") localId: Int): Call<ForecastsForLocationGroup?>?
    suspend fun getWeatherForCity (@Path("localId") localId: Int) : ForecastsForLocationGroup

    @GET("open-data/weather-type-classe.json")
    suspend fun getWeatherTypes(): TypesOfWeatherCollection

}

object IpmaApi {
    val retrofitService : MappedIpmaApiService by lazy {
        retrofit.create(MappedIpmaApiService::class.java) }
}