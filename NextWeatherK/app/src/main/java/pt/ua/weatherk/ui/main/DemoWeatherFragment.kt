package pt.ua.weatherk.ui.main

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import pt.ua.weatherk.databinding.FragmentDemoBinding

class DemoWeatherFragment : Fragment() {

    companion object {
        fun newInstance() = DemoWeatherFragment()
    }

    private lateinit var viewModel: DemoFragmentViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // attach to viewmodel
        viewModel = ViewModelProvider(this).get(DemoFragmentViewModel::class.java)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {

        // use data binding
        val binding = FragmentDemoBinding.inflate(inflater)
        binding.lifecycleOwner = this
        binding.theViewModel = viewModel

        binding.btnGetCities.setOnClickListener {
            viewModel.updateCitiesList()
        }

        binding.btnWeatherDescription.setOnClickListener {
            viewModel.updateWeatherConditionsLabels()
        }
        binding.btnGetForecast.setOnClickListener {
            viewModel.updateForecastForCity()
        }

        return binding.root
    }

}