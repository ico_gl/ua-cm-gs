package pt.ua.weatherk.data

data class CitiesCollectionRoot (
    val owner: String,
    val country: String,
    val data: List<City> )

