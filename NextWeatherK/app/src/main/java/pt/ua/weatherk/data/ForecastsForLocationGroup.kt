package pt.ua.weatherk.data

data class ForecastsForLocationGroup (val globalIdLocal:Int,
                                      val dataUpdate: String,
                                      val data: List<Weather> )