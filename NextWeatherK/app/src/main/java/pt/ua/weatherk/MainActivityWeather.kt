package pt.ua.weatherk

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import pt.ua.weatherk.ui.main.DemoWeatherFragment

class MainActivityWeather : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_activity_weather)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, DemoWeatherFragment.newInstance())
                .commitNow()
        }
    }
}