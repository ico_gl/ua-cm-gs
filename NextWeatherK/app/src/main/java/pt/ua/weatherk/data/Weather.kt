package pt.ua.weatherk.data

data class Weather(
    val forecastDate: String,
    val precipitaProb: Double,
    val tMin: Double,
    val tMax: Double,
    val predWindDir: String,
    val idWeatherType: Int,
    val classWindSpeed: Int
)