package pt.ua.weatherk.data

data class WeatherType (
    val idWeatherType : Int,
    val descWeatherTypePT : String,
    val descWeatherTypeEN: String )
