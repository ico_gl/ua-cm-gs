package pt.ua.roomcontacts.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.List;
import java.util.Objects;

import pt.ua.roomcontacts.R;
import pt.ua.roomcontacts.data.Contact;
import pt.ua.roomcontacts.viewmodel.ContactsViewModel;

public class MainActivity extends AppCompatActivity {

    // reference to to my (view)model
    private ContactsViewModel mContactsViewModel;

    private  ActivityResultLauncher<Intent>  startContactActivityLaucher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView recyclerView = findViewById(R.id.recyclerview);
        final ContactsListAdapter adapter = new ContactsListAdapter(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        // associate a view model to this activity
        mContactsViewModel = new ViewModelProvider(this).get(ContactsViewModel.class);
        // start observing changes in the list (defined in the ViewModel)
        mContactsViewModel.getAllContacts().observe(this, new Observer<List<Contact>>() {
            @Override
            public void onChanged(@Nullable final List<Contact> contacts) {
                adapter.setContacts(contacts);
            }
        });


        //prepare caller of supporting activity
       startContactActivityLaucher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>() {

                    public void onActivityResult(ActivityResult result) {
                        if (result.getResultCode() == Activity.RESULT_OK) {
                            handleNewContactResponse( result.getData());
                        } else {
                        Toast.makeText(
                                getApplicationContext(),
                                R.string.empty_not_saved,
                                Toast.LENGTH_LONG).show();
                    }
                    }
                });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_clear_all) {
                mContactsViewModel.clearAll();
        }
        return super.onOptionsItemSelected(item);
    }


    public void handleNewContactResponse(Intent data) {
        Contact contact = new Contact(
                    Objects.requireNonNull(data.getStringExtra(InputNewContactActivity.EXTRA_TAG_REPLY_DATA_EMAIL)),
                    Objects.requireNonNull(data.getStringExtra(InputNewContactActivity.EXTRA_TAG_REPLY_DATA_NAME))
                    );

            if(  mContactsViewModel.alreadyExists(contact) ) {
                Toast.makeText( this, "Email already exists!", Toast.LENGTH_LONG).show();

            } else {

                // save the new world into the database
                // since the activity has no awareness of the database, it will
                // inform  its supporting viewmodel of the state change
                mContactsViewModel.insert(contact);
            }


    }

    public void doInsertAction(View view) {

        Intent intent = new Intent(this, InputNewContactActivity.class);
        startContactActivityLaucher.launch(intent);

    }
}
